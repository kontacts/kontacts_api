package com.example.kontacts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KontactsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(KontactsApiApplication.class, args);
	}

}

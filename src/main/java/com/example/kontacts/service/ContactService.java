package com.example.kontacts.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.kontacts.model.Contact;
import com.example.kontacts.repository.ContactRepository;

@Service
public class ContactService {

	@Autowired
	private ContactRepository contactRepository;
	
	public List<Contact> findAll() {
		return contactRepository.findAll();
	}

	public Contact add(Contact contact) {
		return contactRepository.save(contact);
	}

	public Optional<Contact> findById(Integer id) {
		return contactRepository.findById(id);
	}
	
	public Contact update(Contact contact) {
		Optional<Contact> found = findById(contact.getId());
		if(!found.isPresent()) {
			return null;
		}
		
		contact.setName(contact.getName() != null ? contact.getName() : found.get().getName());
		contact.setPhone(contact.getPhone() != null ? contact.getPhone() : found.get().getPhone());
		contact.setEmail(contact.getEmail() != null ? contact.getEmail() : found.get().getEmail());
		contact.setUser(found.get().getUser());
		return contactRepository.save(contact);
	}

	public Contact delete(Integer id) {
		Optional<Contact> found = findById(id);
		if(!found.isPresent()) {
			return null;
		}
		contactRepository.delete(found.get());
		return found.get();
	}

}

package com.example.kontacts.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.kontacts.model.User;
import com.example.kontacts.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	public List<User> findAll() {
		return userRepository.findAll();
	}

	public Optional<User> findById(Integer id) {
		return userRepository.findById(id);
	}
	
	public User add(User user) {
		user.setCreationDateTime(new Date());
		return userRepository.save(user);
	}

	public User update(User user) {
		Optional<User> found = findById(user.getId());
		if(!found.isPresent()) {
			return null;
		}
		
		user.setFullName(user.getFullName() != null ? user.getFullName() : found.get().getFullName());
		user.setEmail(user.getEmail() != null ? user.getEmail() : found.get().getEmail());
		user.setCreationDateTime(found.get().getCreationDateTime());
		return userRepository.save(user);
	}	
	
	public User delete(Integer id) {
		Optional<User> found = findById(id);
		if(!found.isPresent()) {
			return null;
		}
		userRepository.delete(found.get());
		found.get().setContacts(null);
		return found.get();
	}

}

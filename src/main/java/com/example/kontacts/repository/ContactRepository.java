package com.example.kontacts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.kontacts.model.Contact;

public interface ContactRepository extends JpaRepository<Contact, Integer> {

}

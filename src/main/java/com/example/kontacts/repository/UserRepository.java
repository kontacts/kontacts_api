package com.example.kontacts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.kontacts.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

}

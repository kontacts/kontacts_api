package com.example.kontacts.controller.request;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @Getter @Setter @AllArgsConstructor @Builder
public class UserRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String fullName;
	
	private String email;

}

package com.example.kontacts.controller.request;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @Getter @Setter @AllArgsConstructor @Builder
public class ContactRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String name;
	
	private String phone;
	
	private String email;
	
	private Integer user;

}

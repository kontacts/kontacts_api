package com.example.kontacts.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.kontacts.controller.request.UserRequest;
import com.example.kontacts.model.User;
import com.example.kontacts.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping
	public ResponseEntity<List<User>> findAll() {
		return ResponseEntity.ok(userService.findAll());
	}
	
	@PostMapping
	public ResponseEntity<User> add(@RequestBody UserRequest user) {
		return ResponseEntity.ok(userService.add(
			User.builder()
				.fullName(user.getFullName())
				.email(user.getEmail())
			.build()
		));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<User> findById(@PathVariable(required = true) Integer id) {
		Optional<User> user = userService.findById(id);
		if(!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(user.get());
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<User> update(@PathVariable(required = true) Integer id, @RequestBody UserRequest user) {
		User updated = userService.update(
			User.builder()
				.id(id)
				.fullName(user.getFullName())
				.email(user.getEmail())
			.build()
		);
		if(updated == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(updated);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<User> delete(@PathVariable(required = true) Integer id) {
		User deleted = userService.delete(id);
		if(deleted == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(deleted);
	}
	
}

package com.example.kontacts.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.kontacts.controller.request.ContactRequest;
import com.example.kontacts.model.Contact;
import com.example.kontacts.model.User;
import com.example.kontacts.service.ContactService;
import com.example.kontacts.service.UserService;

@RestController
@RequestMapping("/contacts")
public class ContactController {

	@Autowired
	private ContactService contactService;
	
	@Autowired
	private UserService userService;
	
	@GetMapping
	public ResponseEntity<List<Contact>> findAll(@RequestParam(value = "user", required = false) Integer useId) {
		if(useId != null) {
			Optional<User> user = userService.findById(useId);
			if(!user.isPresent()) {
				return ResponseEntity.notFound().build();
			}
			return ResponseEntity.ok(user.get().getContacts().stream().collect(Collectors.toList())); 
		}
		return ResponseEntity.ok(contactService.findAll());
	}
	
	@PostMapping
	public ResponseEntity<Contact> add(@RequestBody ContactRequest contact) {
		Optional<User> user = userService.findById(contact.getUser());
		if(!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(contactService.add(
			Contact.builder()
				.name(contact.getName())
				.phone(contact.getPhone())
				.email(contact.getEmail())
				.user(user.get())
			.build()
		));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Contact> findById(@PathVariable(required = true) Integer id) {
		Optional<Contact> contact = contactService.findById(id);
		if(!contact.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(contact.get());
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Contact> update(@PathVariable(required = true) Integer id, @RequestBody ContactRequest contact) {
		Contact updated = contactService.update(
			Contact.builder()
				.id(id)
				.name(contact.getName())
				.phone(contact.getPhone())
				.email(contact.getEmail())
			.build()
		);
		if(updated == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(updated);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Contact> delete(@PathVariable(required = true) Integer id) {
		Contact deleted = contactService.delete(id);
		if(deleted == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(deleted);
	}
	
}

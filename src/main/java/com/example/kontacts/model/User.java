package com.example.kontacts.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "user")
@NoArgsConstructor @Getter @Setter @AllArgsConstructor @Builder @EqualsAndHashCode
public class User implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name = "use_id")
	private Integer id;

	@Column(name = "use_full_name")
	private String fullName;
	
	@Column(name = "use_email")
	private String email;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "use_creation_date_time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd HH:mm:ss")
	@EqualsAndHashCode.Exclude
	private Date creationDateTime;
	
	@OneToMany(mappedBy = "user")
	@Builder.Default
	@EqualsAndHashCode.Exclude
	@JsonManagedReference
	private Set<Contact> contacts = new HashSet<>();
}

package com.example.kontacts.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "contact")
@NoArgsConstructor @Getter @Setter @AllArgsConstructor @Builder @EqualsAndHashCode
public class Contact implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name = "con_id")
	private Integer id;
	
	@Column(name = "con_name")
	private String name;
	
	@Column(name = "con_phone")
	private String phone;
	
	@Column(name = "con_email")
	private String email;
	
	@ManyToOne
	@JoinColumn(name = "use_id")
	@EqualsAndHashCode.Exclude
	@JsonBackReference
	private User user;

}

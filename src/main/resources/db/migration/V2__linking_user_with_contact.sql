CREATE TABLE user ( 
     use_id                 INT NOT NULL auto_increment, 
     use_full_name          VARCHAR(255) NOT NULL, 
     use_email              VARCHAR(255) NOT NULL, 
     use_creation_date_time DATETIME NOT NULL, 
     PRIMARY KEY(use_id) 
);

ALTER TABLE contact
ADD COLUMN use_id int NULL;

ALTER TABLE contact
ADD FOREIGN KEY(use_id) REFERENCES user(use_id);

CREATE TABLE contact ( 
	con_id    INT NOT NULL auto_increment, 
	con_name  VARCHAR(255) NOT NULL, 
	con_phone VARCHAR(30) NOT NULL, 
	con_email VARCHAR(255) NOT NULL, 
	PRIMARY KEY(con_id) 
);

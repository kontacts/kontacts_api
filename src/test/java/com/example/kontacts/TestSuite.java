package com.example.kontacts;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.example.kontacts.controller.ControllerTestSuite;
import com.example.kontacts.service.ServiceTestSuite;

@RunWith(Suite.class)
@SuiteClasses({
	ServiceTestSuite.class,
	ControllerTestSuite.class
})
public class TestSuite {}

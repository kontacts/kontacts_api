package com.example.kontacts.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.http.HttpStatus;
import org.assertj.core.util.Arrays;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.boot.web.server.LocalServerPort;

import com.example.kontacts.KontactsApiApplicationTests;
import com.example.kontacts.controller.request.UserRequest;
import com.example.kontacts.model.User;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserControllerTest extends KontactsApiApplicationTests {

	@LocalServerPort
    private int port;
	
	@Before
	public void setup() throws Exception {
		RestAssured.port = this.port;
	}
	
	@Test
	public void test1Add() {
		UserRequest jsonBody = UserRequest.builder()
									.fullName("Rubem Kalebe")
									.email("rubem@email.com")
									.build();
		
		RestAssured.given()
			.contentType(ContentType.JSON)
			.body(jsonBody)
			.accept(ContentType.JSON)
		.when()
			.post("/users")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_OK)
			.and()
				.body("fullName", equalTo("Rubem Kalebe"))
			.and()
				.body("email", equalTo("rubem@email.com"));
	}
	
	@Test
	public void test2FindAll() {
		User user = User.builder()
				.id(1)
				.fullName("Rubem Kalebe")
				.email("rubem@email.com")
				.build();
		
		User[] users = RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.get("/users")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_OK)
			.and()
				.body("", hasSize(1))
			.and()
				.extract().as(User[].class);
		
		assertEquals(Stream.of(user).collect(Collectors.toList()), Arrays.asList(users));
	}
	
	@Test
	public void test3Update() {
		UserRequest jsonBody = UserRequest.builder()
									.fullName("Rubem Kalebe Santos")
									.email("rubemkalebe@gmail.com")
									.build();
		
		RestAssured.given()
			.contentType(ContentType.JSON)
			.body(jsonBody)
			.accept(ContentType.JSON)
		.when()
			.put("/users/1")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_OK)
			.and()
				.body("fullName", equalTo("Rubem Kalebe Santos"))
			.and()
				.body("email", equalTo("rubemkalebe@gmail.com"));
	}
	
	@Test
	public void test4FindById() {
		User user = User.builder()
				.id(1)
				.fullName("Rubem Kalebe Santos")
				.email("rubemkalebe@gmail.com")
				.build();
		
		User found = RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.get("/users/1")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_OK)
			.and()
				.extract().as(User.class);
		
		assertEquals(user, found);
	}
	
	@Test
	public void test5FindByIdWhenNotExists() {
		RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.get("/users/2")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}
	
	@Test
	public void test6UpdateWhenNotExists() {
		UserRequest jsonBody = UserRequest.builder()
				.fullName("Rubem Kalebe")
				.email("rubemkalebe@gmail.com")
				.build();

		RestAssured.given()
			.contentType(ContentType.JSON)
			.body(jsonBody)
			.accept(ContentType.JSON)
		.when()
			.put("/users/2")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}
	
	@Test
	public void test7DeleteWhenNotExists() {
		RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.delete("/users/2")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}
	
	@Test
	public void test8Delete() {
		User user = User.builder()
				.id(1)
				.fullName("Rubem Kalebe Santos")
				.email("rubemkalebe@gmail.com")
				.build();
		
		User deleted = RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.delete("/users/1")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_OK)
			.and()
				.extract().as(User.class);
		
		assertEquals(user, deleted);
	}
}

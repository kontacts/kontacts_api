package com.example.kontacts.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	UserControllerTest.class,
	ContactControllerTest.class
})
public class ControllerTestSuite {}

package com.example.kontacts.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.http.HttpStatus;
import org.assertj.core.util.Arrays;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;

import com.example.kontacts.KontactsApiApplicationTests;
import com.example.kontacts.controller.request.ContactRequest;
import com.example.kontacts.model.Contact;
import com.example.kontacts.model.User;
import com.example.kontacts.service.UserService;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ContactControllerTest extends KontactsApiApplicationTests {

	@LocalServerPort
    private int port;
	
	@Autowired
	private UserService userService;
	
	@Before
	public void setup() throws Exception {
		RestAssured.port = this.port;
		
		if(!userService.findById(1).isPresent()) {
			User user = User.builder().fullName("Rubem Kalebe").email("rubem@email.com").build();
			userService.add(user);
		}
	}
	
	@Test
	public void test1Add() {
		ContactRequest jsonBody = ContactRequest.builder()
									.name("Rubem")
									.phone("99999-9999")
									.email("rubem@email.com")
									.user(2)
									.build();
		
		RestAssured.given()
			.contentType(ContentType.JSON)
			.body(jsonBody)
			.accept(ContentType.JSON)
		.when()
			.post("/contacts")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_OK)
			.and()
				.body("name", equalTo("Rubem"))
			.and()
				.body("phone", equalTo("99999-9999"))
			.and()
				.body("email", equalTo("rubem@email.com"));
	}
	
	@Test
	public void test2FindAll() {
		Contact contact = Contact.builder()
				.id(1)
				.name("Rubem")
				.phone("99999-9999")
				.email("rubem@email.com")
				.build();
		
		Contact[] contacts = RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.get("/contacts")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_OK)
			.and()
				.body("", hasSize(1))
			.and()
				.extract().as(Contact[].class);
		
		assertEquals(Stream.of(contact).collect(Collectors.toList()), Arrays.asList(contacts));
	}
	
	@Test
	public void test3Update() {
		ContactRequest jsonBody = ContactRequest.builder()
									.name("Rubem Kalebe")
									.phone("+55 31 99999-9999")
									.email("rubemkalebe@gmail.com")
									.build();
		
		RestAssured.given()
			.contentType(ContentType.JSON)
			.body(jsonBody)
			.accept(ContentType.JSON)
		.when()
			.put("/contacts/1")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_OK)
			.and()
				.body("name", equalTo("Rubem Kalebe"))
			.and()
				.body("phone", equalTo("+55 31 99999-9999"))
			.and()
				.body("email", equalTo("rubemkalebe@gmail.com"));
	}
	
	@Test
	public void test4FindById() {
		Contact contact = Contact.builder()
				.id(1)
				.name("Rubem Kalebe")
				.phone("+55 31 99999-9999")
				.email("rubemkalebe@gmail.com")
				.build();
		
		Contact found = RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.get("/contacts/1")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_OK)
			.and()
				.extract().as(Contact.class);
		
		assertEquals(contact, found);
	}
	
	@Test
	public void test5FindByIdWhenNotExists() {
		RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.get("/contacts/2")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}
	
	@Test
	public void test6UpdateWhenNotExists() {
		ContactRequest jsonBody = ContactRequest.builder()
				.name("Rubem Kalebe")
				.phone("+55 31 99999-9999")
				.email("rubemkalebe@gmail.com")
				.build();

		RestAssured.given()
			.contentType(ContentType.JSON)
			.body(jsonBody)
			.accept(ContentType.JSON)
		.when()
			.put("/contacts/2")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}
	
	@Test
	public void test7FindAllByUser() {
		Contact contact = Contact.builder()
				.id(1)
				.name("Rubem Kalebe")
				.phone("+55 31 99999-9999")
				.email("rubemkalebe@gmail.com")
				.build();
		
		Contact[] contacts = RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.get("/contacts?user=2")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_OK)
			.and()
				.body("", hasSize(1))
			.and()
				.extract().as(Contact[].class);
		
		assertEquals(Stream.of(contact).collect(Collectors.toList()), Arrays.asList(contacts));
	}
	
	@Test
	public void test8DeleteWhenNotExists() {
		RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.delete("/contacts/2")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}
	
	@Test
	public void test9Delete() {
		Contact contact = Contact.builder()
				.id(1)
				.name("Rubem Kalebe")
				.phone("+55 31 99999-9999")
				.email("rubemkalebe@gmail.com")
				.build();
		
		Contact deleted = RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.delete("/contacts/1")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_OK)
			.and()
				.extract().as(Contact.class);
		
		assertEquals(contact, deleted);
	}
	
	@Test
	public void test10AddWhenUserNotExists() {
		ContactRequest jsonBody = ContactRequest.builder()
									.name("Rubem")
									.phone("99999-9999")
									.email("rubem@email.com")
									.user(333)
									.build();
		
		RestAssured.given()
			.contentType(ContentType.JSON)
			.body(jsonBody)
			.accept(ContentType.JSON)
		.when()
			.post("/contacts")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}
	
	@Test
	public void test11FindAllWhenUserNotExists() {
		RestAssured.given()
			.accept(ContentType.JSON)
		.when()
			.get("/contacts?user=333")
		.then()
			.assertThat()
				.statusCode(HttpStatus.SC_NOT_FOUND);
	}
	
}

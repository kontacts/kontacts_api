package com.example.kontacts.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	ContactServiceTest.class,
	UserServiceTest.class
})
public class ServiceTestSuite {}

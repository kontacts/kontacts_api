package com.example.kontacts.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.kontacts.model.User;
import com.example.kontacts.repository.UserRepository;

@RunWith(SpringRunner.class)
public class UserServiceTest {

	@TestConfiguration
	static class UserServiceTestContextConfiguration {
		@Bean
		public UserService userService() {
			return new UserService();
		}
	}
	
	@Autowired
	private UserService userService;
	
	@MockBean
	private UserRepository userRepository;
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	
	@Test
	public void findAll() {
		User user = User.builder().id(1).fullName("Rubem Kalebe").email("rubem@email.com").creationDateTime(new Date()).build(); 
		List<User> users = Stream.of(user).collect(Collectors.toList());
		
		when(userRepository.findAll()).thenReturn(users);
		
		assertEquals(users, userService.findAll());
	}
	
	
	@Test
	public void add() {
		User user = User.builder().fullName("Rubem Kalebe").email("rubem@email.com").build();
		
		userService.add(user);
		
		ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);
		verify(userRepository).save(argument.capture());
		
		User saved = argument.getValue();
		assertEquals("Rubem Kalebe", saved.getFullName());
		assertEquals("rubem@email.com", saved.getEmail());
		assertEquals(dateFormat.format(new Date()), dateFormat.format(saved.getCreationDateTime()));
	}
	
	@Test
	public void deleteWhenNotExists() {
		Integer id = 2;
		
		when(userRepository.findById(id)).thenReturn(Optional.empty());
		
		User deleted = userService.delete(id);
		assertNull(deleted);
	}
	
	@Test
	public void deleteWhenExists() {
		Integer id = 1;
		
		when(userRepository.findById(id)).thenReturn(Optional.of(
			User.builder().id(1).fullName("Rubem Kalebe").email("rubem@email.com").creationDateTime(new Date()).build()
		));
		
		User deleted = userService.delete(id);
		assertNotNull(deleted);
	}
	
	@Test
	public void updateWhenNotExists() {
		Integer id = 2;
		
		when(userRepository.findById(id)).thenReturn(Optional.empty());
		
		User updated = userService.update(User.builder().id(id).build());
		assertNull(updated);
	}
	
	@Test
	public void updateNameWhenExists() {
		Integer id = 1;
		
		when(userRepository.findById(id)).thenReturn(Optional.of(
			User.builder().id(1).fullName("Rubem Kalebe").email("rubem@email.com").creationDateTime(new Date()).build()
		));
		
		userService.update(User.builder().id(id).fullName("Rubem Kalebe Santos").build());
		
		ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);
		verify(userRepository).save(argument.capture());
		
		User saved = argument.getValue();
		assertNotNull(saved);
		assertEquals("Rubem Kalebe Santos", saved.getFullName());
		assertEquals("rubem@email.com", saved.getEmail());
	}
	
	@Test
	public void updateEmailWhenExists() {
		Integer id = 1;
		
		when(userRepository.findById(id)).thenReturn(Optional.of(
			User.builder().id(1).fullName("Rubem Kalebe").email("rubem@email.com").creationDateTime(new Date()).build()
		));
		
		userService.update(User.builder().id(id).email("rubemkalebe@email.com").build());
		
		ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);
		verify(userRepository).save(argument.capture());
		
		User saved = argument.getValue();
		assertNotNull(saved);
		assertEquals("Rubem Kalebe", saved.getFullName());
		assertEquals("rubemkalebe@email.com", saved.getEmail());
	}
}

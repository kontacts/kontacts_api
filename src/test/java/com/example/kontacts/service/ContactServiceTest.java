package com.example.kontacts.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.kontacts.model.Contact;
import com.example.kontacts.repository.ContactRepository;

@RunWith(SpringRunner.class)
public class ContactServiceTest {

	@TestConfiguration
	static class ContactServiceTestContextConfiguration {
		@Bean
		public ContactService contactService() {
			return new ContactService();
		}
	}
	
	@Autowired
	private ContactService contactService;
	
	@MockBean
	private ContactRepository contactRepository;
	
	@Test
	public void findAll() {
		List<Contact> contacts = Stream.of(
			Contact.builder().id(1).name("Rubem").phone("99999-9999").email("rubem@email.com").build()
		).collect(Collectors.toList());
		
		when(contactRepository.findAll()).thenReturn(contacts);
		
		assertEquals(contacts, contactService.findAll());
	}
	
	@Test
	public void add() {
		Contact contact = Contact.builder().id(1).name("Rubem").phone("99999-9999").email("rubem@email.com").build();
		
		contactService.add(contact);
		
		ArgumentCaptor<Contact> argument = ArgumentCaptor.forClass(Contact.class);
		verify(contactRepository).save(argument.capture());
		
		Contact saved = argument.getValue();
		assertEquals("Rubem", saved.getName());
		assertEquals("99999-9999", saved.getPhone());
		assertEquals("rubem@email.com", saved.getEmail());
	}
	
	@Test
	public void deleteWhenNotExists() {
		Integer id = 2;
		
		when(contactRepository.findById(id)).thenReturn(Optional.empty());
		
		Contact deleted = contactService.delete(id);
		assertNull(deleted);
	}
	
	@Test
	public void deleteWhenExists() {
		Integer id = 1;
		
		when(contactRepository.findById(id)).thenReturn(Optional.of(
				Contact.builder().id(1).name("Rubem").phone("99999-9999").email("rubem@email.com").build()
		));
		
		Contact deleted = contactService.delete(id);
		assertNotNull(deleted);
	}
	
	@Test
	public void updateWhenNotExists() {
		Integer id = 2;
		
		when(contactRepository.findById(id)).thenReturn(Optional.empty());
		
		Contact updated = contactService.update(Contact.builder().id(id).build());
		assertNull(updated);
	}
	
	@Test
	public void updateNameWhenExists() {
		Integer id = 1;
		
		when(contactRepository.findById(id)).thenReturn(Optional.of(
				Contact.builder().id(1).name("Rubem").phone("99999-9999").email("rubem@email.com").build()
		));
		
		contactService.update(Contact.builder().id(id).name("Rubem Kalebe").build());
		
		ArgumentCaptor<Contact> argument = ArgumentCaptor.forClass(Contact.class);
		verify(contactRepository).save(argument.capture());
		
		Contact saved = argument.getValue();
		assertNotNull(saved);
		assertEquals("Rubem Kalebe", saved.getName());
		assertEquals("99999-9999", saved.getPhone());
		assertEquals("rubem@email.com", saved.getEmail());
	}
	
	@Test
	public void updatePhoneWhenExists() {
		Integer id = 1;
		
		when(contactRepository.findById(id)).thenReturn(Optional.of(
				Contact.builder().id(1).name("Rubem").phone("99999-9999").email("rubem@email.com").build()
		));
		
		contactService.update(Contact.builder().id(id).phone("88888-8888").build());
		
		ArgumentCaptor<Contact> argument = ArgumentCaptor.forClass(Contact.class);
		verify(contactRepository).save(argument.capture());
		
		Contact saved = argument.getValue();
		assertNotNull(saved);
		assertEquals("Rubem", saved.getName());
		assertEquals("88888-8888", saved.getPhone());
		assertEquals("rubem@email.com", saved.getEmail());
	}
	
	@Test
	public void updateEmailWhenExists() {
		Integer id = 1;
		
		when(contactRepository.findById(id)).thenReturn(Optional.of(
				Contact.builder().id(1).name("Rubem").phone("99999-9999").email("rubem@email.com").build()
		));
		
		contactService.update(Contact.builder().id(id).email("rubem@email.com.br").build());
		
		ArgumentCaptor<Contact> argument = ArgumentCaptor.forClass(Contact.class);
		verify(contactRepository).save(argument.capture());
		
		Contact saved = argument.getValue();
		assertNotNull(saved);
		assertEquals("Rubem", saved.getName());
		assertEquals("99999-9999", saved.getPhone());
		assertEquals("rubem@email.com.br", saved.getEmail());
	}
	
}

package com.example.kontacts;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.MySQLContainer;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {KontactsApiApplicationTests.Initializer.class})
public class KontactsApiApplicationTests {

	public static MySQLContainer<?> mysql = new MySQLContainer<>("mysql:5.7")
			.withDatabaseName("kontacts")
			.withUsername("kontactsUser")
			.withPassword("kontactsUser");
	
	static {
		mysql.start(); // prevents JUnit of trying to create two instances
	}
	
	static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		@Override
		public void initialize(ConfigurableApplicationContext applicationContext) {
			TestPropertyValues.of(
				"spring.datasource.url=" + mysql.getJdbcUrl(),
				"spring.datasource.username=" + mysql.getUsername(),
				"spring.datasource.password=" + mysql.getPassword(),
				"spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver"
			).applyTo(applicationContext.getEnvironment());
		}
	}
	
	@Test
	public void contextLoads() {
	}

}

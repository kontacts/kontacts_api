# Kontacts API


## Requirements
* Java JDK 8
* Git
* **Eclipse IDE for Java EE Developers** with STS Plugin or **STS IDE**
* Docker
* Docker Compose
* Lombok
    * For installing Lombok, first download the *.jar* file on https://projectlombok.org/download
    * Run the *.jar* file (you can use `java -jar lombok.jar`). Remember to install it with your IDE closed
    * Specify where your IDE is located, if it wasn't located automatically
    * Click on *Install/Update* (the installation is very fast)
    * Maybe you'll need to clean the project (*Project > Clean*)


## Getting started
1. Clone this project.
2. Import the project into Eclipse/STS as ***Existing Gradle Project***.
3. Run `docker-compose -f db-docker.yml up -d` for starting a new MySQL container.
4. Run the project with your IDE.


## Working with Docker images
1. For building a image use `gradle build docker -x test`.
2. For pushing an image, first login to GitLab registry with `docker login registry.gitlab.com`.
3. After entered your credentials you can push an image using `gradle build docker dockerPush -x test`.


## Testing the API
* **Fetch all registers:**
```bash
curl http://localhost:8080/contacts
```
* **Create a new contact:**
```bash
curl -X POST -H "Content-Type: application/json" -d '{"name": "Rubem", "phone": "+55 31 99999-9999", "email": "rubemkalebe@gmail.com"}' http://localhost:8080/contacts
```
* **Find a contact by id:**
```bash
curl  http://localhost:8080/contacts/1
```
* **Update a contact by id:**
```bash
curl -X PUT -H "Content-Type: application/json" -d '{"name": "Rubem Kalebe"}' http://localhost:8080/contacts/1
```
* **Remove a contact:**
```bash
curl -X DELETE http://localhost:8080/contacts/1
```
